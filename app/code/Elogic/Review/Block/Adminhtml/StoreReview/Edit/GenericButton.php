<?php

namespace Elogic\Review\Block\Adminhtml\StoreReview\Edit;

use Magento\Backend\Block\Widget\Context;
use Elogic\Review\Api\StoreReviewRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var StoreReviewRepositoryInterface
     */
    protected $storeReviewRepository;

    /**
     * @param Context $context
     * @param StoreReviewRepositoryInterface $storeReviewRepository
     */
    public function __construct(
        Context $context,
        StoreReviewRepositoryInterface $storeReviewRepository
    ) {
        $this->context = $context;
        $this->storeReviewRepository = $storeReviewRepository;
    }

    /**
     * Return review ID
     *
     * @return int|null
     */
    public function getReviewId()
    {
        if ($this->context->getRequest()->getParam('review_id'))
        {
            try {
                return $this->storeReviewRepository->getById(
                    $this->context->getRequest()->getParam('review_id')
                )->getId();
            } catch (NoSuchEntityException $e) {
            }
        }

        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
