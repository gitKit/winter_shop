<?php

namespace Elogic\Review\Block;

use Magento\Framework\View\Element\Template;

use Elogic\Review\Model\ResourceModel\StoreReview\CollectionFactory as storeReviewCollectionFactory;

class Display extends Template
{
    /**
     * @var StoreReviewCollectionFactory
     */
    protected $storeReviewCollectionFactory;

    /**
     * Display constructor.
     * @param Template\Context $context
     * @param StoreReviewCollectionFactory $storeReviewCollectionFactory
     */
    public function __construct(
        Template\Context $context,
        storeReviewCollectionFactory $storeReviewCollectionFactory
    ) {
        parent::__construct($context);
        $this->storeReviewCollectionFactory = $storeReviewCollectionFactory;
    }

    public function getReviewCollection()
    {
        $reviewCollection = $this->storeReviewCollectionFactory->create();
        $reviewCollection->addFieldToFilter('is_active', 1);
        return $reviewCollection;
    }
}
