<?php

namespace Elogic\Review\Api;

/**
 * Interface StoreReviewRepositoryInterface
 * @package Elogic\Review\Api
 */
interface StoreReviewRepositoryInterface
{
    /**
     * Save store review.
     *
     * @param \Elogic\Review\Api\Data\StoreReviewInterface $storeReview
     * @return \Elogic\Review\Api\Data\StoreReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\StoreReviewInterface $storeReview);

    /**
     * Retrieve store review.
     *
     * @param int $storeReviewId
     * @return \Elogic\Review\Api\Data\StoreReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeReviewId);

    /**
     * Delete store review.
     *
     * @param \Elogic\Review\Api\Data\StoreReviewInterface $storeReview
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\StoreReviewInterface $storeReview);

    /**
     * Delete store review by ID.
     *
     * @param int $storeReviewId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storeReviewId);
}