<?php

namespace Elogic\Review\Model;

use Elogic\Review\Api\Data\StoreReviewInterface;
use Magento\Framework\Model\AbstractModel;

class StoreReview extends AbstractModel implements StoreReviewInterface
{
    /**#@+
     * Review's statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Construct.
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Elogic\Review\Model\ResourceModel\StoreReview::class);
    }

    /**
     * Get ID
     *
     * @return int|null;
     */
    public function getId()
    {
        return $this->getData(self::REVIEW_ID);
    }

    /**
     * Get customer name
     *
     * @return string|null
     */
    public function getCustomerName()
    {
        return $this->getData(self::CUSTOMER_NAME);
    }

    /**
     * Get content
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * is Active
     *
     * @return int|null
     */
    public function isActive()
    {
        return $this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return StoreReviewInterface
     */
    public function setId($id)
    {
        return $this->setData(self::REVIEW_ID, $id);
    }

    /**
     * Set customer id
     *
     * @param string $customerName
     * @return StoreReviewInterface
     */
    public function setCustomerName($customerName)
    {
        return $this->setData(self::CUSTOMER_NAME, $customerName);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return StoreReviewInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set creation time
     *
     * @param string $creationTime
     * @return StoreReviewInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * Set update time
     *
     * @param string $updateTime
     * @return StoreReviewInterface
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $$updateTime);
    }

    /**
     * Set is active
     *
     * @param int $isActive
     * @return StoreReviewInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }
}