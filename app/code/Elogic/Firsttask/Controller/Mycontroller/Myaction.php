<?php

namespace Elogic\Firsttask\Controller\Mycontroller;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Myaction extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    public function execute()
    {
        echo "Mycontroller myaction";
    }
}