<?php

namespace Elogic\Firsttask\Controller\Mycontroller;

use Elogic\Review\Api\StoreReviewRepositoryInterface;
use Elogic\Review\Model\StoreReview;
use Elogic\Review\Model\StoreReviewFactory;
use Elogic\Review\Api\Data\StoreReviewInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Index extends Action implements HttpGetActionInterface, HttpPostActionInterface
{

    protected $storeReviewFactory;
    protected $storeReviewRepository;
    protected $storeReview;


    public function __construct(
        StoreReviewFactory $storeReviewFactory,
        StoreReviewRepositoryInterface $storeReviewRepository,
        StoreReviewInterface $storeReview,
        Context $context
    )
    {
        $this->storeReviewFactory = $storeReviewFactory;
        $this->storeReviewRepository = $storeReviewRepository;
        $this->storeReview = $storeReview;
        parent::__construct($context);
    }


    public function execute()
    {
//        for($i =20; $i<30; $i++ )
//        {
//            $review = $this->storeReviewFactory->create();
//            $review->setCustomerName("my customer ".$i )->setContent("review ".$i);
//            $this->storeReviewRepository->save($review);
//            unset($review);
//        }


//        $review = $this->storeReviewRepository->getById(2);
//
//        echo $review->getContent();
//

//        echo $review->getId();

//        $review = $this->storeReviewFactory->create();
//        $review = $this->storeReviewRepository->getById(2);
//        $review->setIsActive(1);
//        $review = $this->storeReviewRepository->save($review);


        /** @var \Elogic\Review\Model\StoreReview $storeReview */
        $storeReview = $this->storeReview;
        $collection = $storeReview->getCollection();


        $collection->addFieldToSelect('customer_name');

        $collection->addFieldToFilter(
            ['review_id', 'customer_name'],
            [['eq' => 8], ['eq' => 'my customer 20']]
        );

//        $collection->addFieldToFilter('customer_name', ['eq' => 'my customer 20']);
//


        $collection->setOrder('review_id', \Zend_Db_Select::SQL_ASC);

        $collection->getSelect()->reset(\Zend_Db_Select::COLUMNS)->columns("review_id");

//        $collection->load();

        foreach ($collection as $item)
        {
            echo get_class($item);
            var_dump($item->getData());
        }

        echo $collection->getSelect()->__toString();


        die();


        /** @var StoreReview $review */
//        $review = $collection->getFirstItem();
//        $review->setReviewTitle("Cool store");
//        $review->setCustomerName("Awesome customer");
//        $data["review_title"] = "Cool store";
//        $review->setData($data);
//        $this->storeReviewRepository->save($review);
//        echo ($review->getData('review_id'));
    }
}