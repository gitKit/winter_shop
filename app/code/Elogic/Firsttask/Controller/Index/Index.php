<?php

namespace Elogic\Firsttask\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Elogic\Review\Helper\Data as StoreReviewHelper;

class Index extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var StoreReviewHelper
     */
    private $storeReviewHelper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory resultPageFactory§
     * @param StoreReviewHelper $storeReviewHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        StoreReviewHelper $storeReviewHelper

    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
        $this->storeReviewHelper = $storeReviewHelper;
    }

    /**
     * @return \Magento\Framework\View\Result\LayoutFactory
     */
    public function execute()
    {
        if ($this->storeReviewHelper->getStoreReviewEnabled()) {
            $resultLayout = $this->resultPageFactory->create();
            return $resultLayout;
        } else {
            die("store reviewing is not allowed");
        }
    }
}