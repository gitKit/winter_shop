<?php

namespace Elogic\Firsttask\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\View\Element\Template;

class Firsttask extends Template
{
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Firsttask constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->productRepository = $productRepository;
    }

    /**
     * @return null|string
     */
    public function getFirstProductName()
    {
        $product = $this->productRepository->getById(1);
        $product->setData('foo', 'bar');
        $foo = $product->getData('foo');
        $product->setFoo('bar');
        $foo = $product->getFoo();
        if ($product->getId()){
            return $product->getName();
        }
        return '';
    }
}